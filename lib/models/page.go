package models

import (
	"database/sql"
	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday"
	"html/template"
	"time"
)

type Page struct {
	Title, Slug, Contents string
	Posted, Updated       time.Time
}

func (p Page) HtmlContents() template.HTML {
	contents := bluemonday.UGCPolicy().SanitizeBytes(blackfriday.MarkdownCommon([]byte(p.Contents)))
	return template.HTML(contents)
}

type Pages []Page

func (p Pages) Len() int {
	return len(p)
}
func (p Pages) Less(i, j int) bool {
	return p[i].Updated.After(p[j].Updated)
}
func (p Pages) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func LoadPages(rows *sql.Rows) (Pages, error) {
	pages := Pages{}
	for rows.Next() {
		var page Page
		err := rows.Scan(&page.Title, &page.Slug, &page.Contents, &page.Posted, &page.Updated)
		if err != nil {
			return nil, err
		}
		pages = append(pages, page)
	}
	return pages, nil
}

func LoadPage(d *sql.DB, slug string) (Pages, error) {
	rows, err := d.Query("SELECT title, slug, contents, posted, updated FROM pages WHERE slug = $1", slug)
	if err != nil {
		return nil, err
	}
	return LoadPages(rows)
}

func InsertPage(d *sql.DB, page Page) error {
	_, err := d.Exec("INSERT INTO pages (title, slug, contents) VALUES ($1, $2, $3)", page.Title, page.Slug, page.Contents)
	return err
}

func UpdatePage(d *sql.DB, page Page) error {
	_, err := d.Exec("UPDATE pages SET title = $1, slug = $2, contents = $3, updated = now() WHERE slug = $2", page.Title, page.Slug, page.Contents)
	return err
}
