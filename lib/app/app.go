package app

import (
	"bitbucket.org/georgerogers42/maximgo/lib/models"
	"database/sql"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"html/template"
	"net/http"
	"os"
)

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		panic(err)
	}
}

var App = mux.NewRouter()

func init() {
	App.HandleFunc("/", viewPage).Methods("GET")
	App.HandleFunc("/page/{slug}", viewPage).Methods("GET")
	App.HandleFunc("/page/new/{slug}", insertPage).Methods("POST")
	App.HandleFunc("/page/update/{slug}", updatePage).Methods("POST")
}

type Data struct {
	Pages models.Pages
	Slug  string
}

var baseTpl = template.Must(template.ParseFiles("templates/base.html"))

var indexTpl = template.Must(baseTpl.ParseFiles("templates/index.html"))

func viewPage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug, ok := vars["slug"]
	if !ok {
		slug = "index"
	}
	page, err := models.LoadPage(db, slug)
	if err != nil {
		http.Error(w, "Error 500", 500)
		return
	}
	data := Data{Pages: page, Slug: slug}
	err = indexTpl.Execute(w, data)
	if err != nil {
		panic(err)
	}
}

func insertPage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug := vars["slug"]
	page := models.Page{Title: r.FormValue("title"), Slug: slug, Contents: r.FormValue("contents")}
	err := models.InsertPage(db, page)
	if err != nil {
		http.Error(w, "Error 500", 500)
		return
	}
	http.Redirect(w, r, "/page/"+slug, 302)
}

func updatePage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug := vars["slug"]
	page := models.Page{Title: r.FormValue("title"), Slug: slug, Contents: r.FormValue("contents")}
	err := models.UpdatePage(db, page)
	if err != nil {
		http.Error(w, "Error 500", 500)
		return
	}
	http.Redirect(w, r, "/page/"+slug, 302)
}
