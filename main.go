package main

import (
	"bitbucket.org/georgerogers42/maximgo/lib/app"
	"flag"
	"log"
	"net/http"
)

func main() {
	on := flag.String("o", "0.0.0.0:8080", "Listen address")
	flag.Parse()
	http.Handle("/static/", http.FileServer(http.Dir("public")))
	http.Handle("/", app.App)
	log.Println("Listening on:", *on)
	log.Fatal(http.ListenAndServe(*on, nil))
}
