SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: pages; Type: TABLE; Schema: public; Owner: gfr
--

CREATE TABLE pages (
    slug character varying NOT NULL,
    title character varying NOT NULL,
    contents text NOT NULL,
    posted timestamp without time zone DEFAULT '2016-10-05 10:23:50.425312'::timestamp without time zone NOT NULL,
    updated timestamp without time zone DEFAULT '2016-10-05 10:36:50.935542'::timestamp without time zone NOT NULL
);

ALTER TABLE ONLY pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (slug);
